using UnityEngine;
using CreatorKitCode;

public class SpawnerSample : MonoBehaviour
{
    public GameObject ObjectToSpawn;

    void Start()
    {
        //Manual Test Code

        //SpawnPotion(0);
        //SpawnPotion(45);
        //SpawnPotion(90);
        //SpawnPotion(135);

        //Main
        LootAngle myLootAngle = new LootAngle(25);
        for (int i = 0; i < 3; i++)
        {
            SpawnPotion(myLootAngle.NextAngle());
            //SpawnPotion(Random.Range(0, 270));
        }
    }

    void SpawnPotion (int angle)
    {
        int radius = 5;

        Vector3 direction = Quaternion.Euler(0, angle, 0) * Vector3.right;
        Vector3 spawnPosition = transform.position + direction * radius;
        Instantiate(ObjectToSpawn, spawnPosition, Quaternion.identity);
    }
}

public class LootAngle
{
    int angle;
    int step;


    public LootAngle(int increment)
    {
        step = increment;
        angle = 0;
    }

    public int NextAngle()
    {
        int currentAngle = angle;
        angle = Helpers.WrapAngle(angle + step);

        return currentAngle;
    }
}
